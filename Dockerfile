# ****************************************
# Build Stage
# ****************************************
# FROM node:14-alpine3.14 as build-image

# RUN apk update && \
#     apk add openssl

# RUN openssl genrsa -des3 -passout pass:x333 -out key.pem 2048
# RUN cp key.pem key.pem.orig
# RUN openssl rsa -passin pass:x333 -in key.pem.orig -out key.pem
# RUN openssl req -new -key key.pem -out cert.csr -subj "/C=US/ST=TX/L=San Antonio/O=Appddiction Studio LLC/OU=Digital/CN=default"
# RUN openssl x509 -req -days 3650 -in cert.csr -signkey key.pem -out cert.pem

# ****************************************
# Deploy Stage
# ****************************************
# FROM nginx

# COPY --from=build-image . /usr/share/nginx//html
# COPY . /usr/share/nginx/html

# COPY --from=build-image cert.pem /etc/ssl/certs/cert.pem
# COPY --from=build-image key.pem /etc/ssl/private/key.pem

# COPY nginx.conf /etc/nginx/conf.d/default.conf

# EXPOSE 80
# EXPOSE 443

FROM nginx:stable

RUN mkdir -p /usr/src/jobsAtScaleApp

COPY . /usr/src/jobsAtScaleApp
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

RUN mkdir -p /var/www/html
RUN cp -r //usr/src/jobsAtScaleApp/* /var/www/html

RUN nginx